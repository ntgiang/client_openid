from flask import Flask, render_template, json, request
import requests
app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template("demo.html")
    #return render_template("demo2.html")


@app.route('/get_info', methods=["POST"])
def get_info():
    access_token = request.values["access_token"]
    response = requests.get("http://id.app360.vn/sdk/me?access_token=%s" % access_token)
    return response.text


if __name__ == '__main__':
    app.debug = True
    app.run(port=60000)
